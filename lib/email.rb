class Email
  include ActiveModel::Validations
  attr_reader :address, :subject, :message

  def initialize(options)
    @address = options[:address]
    @subject = options[:subject]
    @message = options[:message]
  end

  def send_email
    params = {
      :from => USER_EMAIL,
      :to => @address,
      :subject => @subject,
      :text => @message
        }
    conn = Faraday.new(:url => 'https://api.mailgun.net/v2') do |faraday|
      faraday.request  :url_encoded
      #faraday.response :logger
      faraday.adapter  Faraday.default_adapter 
    end
    conn.basic_auth('api', API_KEY)
    conn.post("#{DOMAIN_NAME}.mailgun.org/messages", params)
  end


end

