require 'spec_helper'

describe Unsubscriber do 

  let(:unsubscriber) {Unsubscriber.new(address: 'mfpiccolo@gmail.com')}

  context '#add_unsubscriber' do 
    it 'adds an email to the unsubscribe list' do 
      stub = stub_request(:post, "https://api:#{API_KEY}@api.mailgun.net/v2/#{DOMAIN_NAME}.mailgun.org/unsubscribes")
      unsubscriber.add_unsubscriber('mfpiccolo@gmail.com')
      stub.should have_been_requested
    end

  end

  context '#view_unsubscribers' do
    it 'requests the list of unsubscribers' do
      stub = stub_request(:get, "https://api:#{API_KEY}@api.mailgun.net/v2/#{DOMAIN_NAME}.mailgun.org/unsubscribes")
      unsubscriber.view_unsubscribers
      stub.should have_been_requested
    end


    it 'gives a list of everyone (name and email) who has been unsubscribed' do 
      VCR.use_cassette('view_unsubscribers') do
        unsubscriber.view_unsubscribers.list.should eq ""
      end
    end
  end
end
