class Unsubscriber

  def initialize(options)
    
  end

  def add_unsubscriber(address)
    params = {
      :address => address,
      :tag => '*'
          }
    conn = Faraday.new(:url => 'https://api.mailgun.net/v2') do |faraday|
      faraday.request  :url_encoded
      #faraday.response :logger
      faraday.adapter  Faraday.default_adapter 
    end
    conn.basic_auth('api', API_KEY)
    conn.post("#{DOMAIN_NAME}.mailgun.org/unsubscribes", params)
  end

  def view_unsubscribers
    conn = Faraday.new(:url => 'https://api.mailgun.net/v2') do |faraday|
      faraday.request  :url_encoded
      #faraday.response :logger
      faraday.adapter  Faraday.default_adapter 
    end
    conn.basic_auth('api', API_KEY)
    conn.get("#{DOMAIN_NAME}.mailgun.org/unsubscribes")
  end




end
