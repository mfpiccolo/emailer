require 'faraday'
require 'active_model'
require './env'
require './lib/email'


def welcome
  puts "\n\nWelcome to the Mailgun command-line email client."
  menu
end



def menu
  choice = nil
  until choice == 'e'
    puts "\n\nWhat would you like to do?"
    puts "Press 's' to send and email or 'm' to manage subscriptions"
    puts "Press 'e' to exit"

  case choice = gets.chomp
    when 's'
      send_ui
    when 'm'
      manage_subscriptions
    when 'e'
      exit
    else
      invalid
    end
  end
end

def send_ui
  puts "\nEnter email address you would like to send to."
  address = gets.chomp
  puts "\nEnter the subject:"
  subject = gets.chomp
  puts "\nEnter the message:"
  message = gets.chomp
  email = Email.new(address: address, subject: subject, message: message)
  response = email.send_email
  puts "\n Your email was sent!"
end
welcome