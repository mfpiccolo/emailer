require 'active_model'
require 'rspec'
require 'faraday'
require 'uri'
require 'json'
require 'vcr'
require 'webmock/rspec'
require 'vcr'

require 'email'
require './env'
require 'unsubscriber'




VCR.configure do |c|
  c.cassette_library_dir = 'spec/vcr_cassettes'
  c.hook_into :webmock 
end
